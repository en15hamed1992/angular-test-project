import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SelectOption } from '../../models/select.option';

@Component({
  selector: 'app-basic-select',
  templateUrl: './basic-select.component.html',
  styleUrls: ['./basic-select.component.css']
})
export class BasicSelectComponent implements OnInit {

  selectedValue='';
  @Input() loading=false;
  @Input() label=''
  @Input() options:SelectOption<any>[]=[]

  @Output() valueChanged = new EventEmitter<any>();

 constructor() {}
  ngOnInit() {
  }
  updateValue($event:any){
    this.valueChanged.emit($event);
     
   }
}
