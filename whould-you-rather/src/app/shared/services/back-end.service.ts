import {
  AddQuestion,
  Question,
  QuestionDetails,
  Questions,
  SaveQuestionAnswer,
} from './../../pages/questions/models/question';
import { Injectable } from '@angular/core';
import { from, Observable, map, mergeMap } from 'rxjs';
import { User } from 'src/app/pages/auth/models';

declare function _getUsers(): Promise<User>;
declare function _getQuestions(): Promise<Questions>;
declare function _saveQuestion (question:any): Promise<Question>;
declare function _saveQuestionAnswer
(autheduser:string,qid:string,answer:string): Promise<void>;

@Injectable({
  providedIn: 'root',
})
export class BackEndService {
  constructor() {}
  FetchUsers(): Observable<User> {
    return from(_getUsers());
  }

  FetchQuestions(): Observable<Questions> {
    return from(_getQuestions());
  }
  FetchQuestionDetials(id: string): Observable<QuestionDetails | null> {
    return from(_getQuestions()).pipe(
      map((questions) => questions[id]),
      mergeMap((question) =>
        this.FetchUsers().pipe(
          map((users) => users[question.author]),
          map((user) => {
            return {
              id: question.id,
              author: question.author,
              avatarURL: user.avatarURL,
              optionOne: question.optionOne,
              optionTow: question.optionTwo,
            };
          })
        )
      )
    );
  }

  VoteForAnswer(slectedAnswer: SaveQuestionAnswer): Observable<any> {   
    return from(_saveQuestionAnswer
      (slectedAnswer.user.id,slectedAnswer.questionId,slectedAnswer.answer));
  }
  AddQuestion(question:AddQuestion):Observable<Question>{   
    return from(_saveQuestion(question));
  }
}
