import { LoadingModalComponent } from './components/loading-modal/loading-modal.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BasicSelectComponent } from 'src/app/shared/components/basic-select/basic-select.component';
import { MaterialModule } from '../matrial';

export const COMPONENTS = [BasicSelectComponent,LoadingModalComponent];
@NgModule({
  imports: [CommonModule, MaterialModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class SharedModule {}
