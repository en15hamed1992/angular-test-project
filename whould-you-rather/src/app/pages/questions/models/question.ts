import { UserDetails } from 'src/app/pages/auth/models';

export interface Questions {
  [id: string]: Question;
}
export interface Question {
  id: string;
  author: string;
  timestamp: number;
  optionOne: QuestionOption;
  optionTwo: QuestionOption;
}
export interface QuestionDetails {
  id: string;
  author: string;
  avatarURL: string;
  optionOne: QuestionOption;
  optionTow: QuestionOption;
}

export interface QuestionOption {
  votes: string[];
  text: string;
}
export interface SaveQuestionAnswer {
  user: UserDetails;
  questionId: string;
  answer: string;
}
export interface AddQuestion extends AddOptions {
  author: string;
}

export interface AddOptions {
  optionOneText: string;
  optionTwoText: string;
}
