import { createAction, props } from '@ngrx/store';
import { UserDetails } from '../../auth/models';
const actionskey = '[UnAnswerd Questions Page]';
export const enter = createAction(
  `${actionskey} Enter`,
  props< {user: UserDetails} >()
);
