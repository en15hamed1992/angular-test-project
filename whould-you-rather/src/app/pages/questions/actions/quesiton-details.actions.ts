import { UserDetails } from './../../auth/models/user';
import { SelectOption } from 'src/app/shared/models/select.option';
import { SaveQuestionAnswer } from './../models/question';
import { createAction, props } from '@ngrx/store';

const actionskey = '[Question Details Page]';
export const enter = createAction(
  `${actionskey} Enter`,
  props<{ id: string }>()
);

export const vote = createAction(
  `${actionskey} Vote`,
  props<{
    selectedAnswer: SaveQuestionAnswer,
    accounts: SelectOption<UserDetails>[]
  }>()
);