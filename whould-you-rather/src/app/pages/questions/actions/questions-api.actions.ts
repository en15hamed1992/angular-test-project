import {
  QuestionDetails,
  SaveQuestionAnswer,
  AddQuestion,
} from './../models/question';
import { Question } from '../models/question';
import { createAction, props } from '@ngrx/store';
import { SelectOption } from 'src/app/shared/models/select.option';
import { UserDetails } from '../../auth/models';

const unanswerdActionskey = '[Unanswerd Questions/API]';

export const FetchUnanswerdQuestionsSucess = createAction(
  `${unanswerdActionskey} Fetch Unanswerd Questions Sucess`,
  props<{ questions: Question[] }>()
);

export const FetchUnanswerdQuestionsFailer = createAction(
  `${unanswerdActionskey} Fetch Unanswerd Questions Failer`,
  props<{ error: any }>()
);

const AanswerdActionskey = '[Aanswerd Questions/API]';

export const FetchAanswerdQuestionsSucess = createAction(
  `${AanswerdActionskey} Fetch Aanswerd Questions Sucess`,
  props<{ questions: Question[] }>()
);

export const FetchAanswerdQuestionsFailer = createAction(
  `${AanswerdActionskey} Fetch Aanswerd Questions Failer`,
  props<{ error: any }>()
);

const QuestionDetailskey = '[Question Details/API]';

export const FetchQuestionDetailsSucess = createAction(
  `${QuestionDetailskey} Fetch Question Details Sucess`,
  props<{ question: QuestionDetails }>()
);

export const FetchQuestionDetailsFailer = createAction(
  `${QuestionDetailskey} Fetch Question Details Failer`,
  props<{ error: any }>()
);

export const FetchQuestionDetailsNotFound = createAction(
  `${QuestionDetailskey}  Question Not Found `
);

export const SaveQuestionAnswerSucess = createAction(
  `${QuestionDetailskey} Save Question Answer Sucess`,
  props<{
    selectedAnswer: SaveQuestionAnswer;
    accounts: SelectOption<UserDetails>[];
  }>()
);

export const SaveQuestionAnswerFailer = createAction(
  `${QuestionDetailskey} Save Question Answer Failer`,
  props<{ error: any }>()
);

const AddQuestionkey = '[Add Question/API]';

export const SaveQuestionSucess = createAction(
  `${AddQuestionkey} Save Question  Sucess`,
  props<{ question: Question }>()
);

export const SaveQuestionFailer = createAction(
  `${AddQuestionkey} Save Question  Failer`,
  props<{ error: any }>()
);
