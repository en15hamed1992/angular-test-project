import { AddQuestion } from './../models/question';
import { createAction, props } from '@ngrx/store';

const actionskey='[Add Question Page]';

export const Add = createAction(
    `${actionskey} Add Question`,
    props<{question:AddQuestion}>());