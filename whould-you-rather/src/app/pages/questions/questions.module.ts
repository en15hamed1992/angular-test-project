import { QuestionTabelComponent } from './components/questions-table.component';
import { AddQuestionEffects } from './effects/add-question.effects';
import { QuestionDetailsEffects } from './effects/question-details.effects';
import { QuestionsRoutingModule } from './questions-routing.module';
import { QuestionsEffects } from './effects/questions.effects';
import { MaterialModule } from '../../matrial/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionsPageComponent } from './containers/questions-page.component';
import * as fromQuestions from './reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AnswerdQuestionsPageComponent } from './containers/answerd-questions-page.component';
import { UnanswerdQuestionsPageComponent } from './containers/unanswerd-questions-page.component';
import { QuestionDetailsPageComponent } from './containers/question-details-page.component';
import { QuestionDetailComponent } from './components/question-details.component';
import { UnanswerdQuestionDetailsComponent } from './components/unanswerd-question-details.component';
import { AnswerdQuestionDetailsComponent } from './components/answerd-question-details.component';
import { AddQuestionComponent } from './containers/add-questions.component';
import { AddQuestionFormComponent } from './components/add-question-form.component';


export const COMPONENTS = [
  QuestionsPageComponent,
  AnswerdQuestionsPageComponent,
  UnanswerdQuestionsPageComponent,
  QuestionDetailsPageComponent,
  QuestionDetailComponent,
  UnanswerdQuestionDetailsComponent,
  AnswerdQuestionDetailsComponent,
  AddQuestionComponent,
  AddQuestionFormComponent,
  QuestionTabelComponent
];
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    QuestionsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forFeature({
     name:fromQuestions.questionsFetureKey,
     reducer:fromQuestions.reducers
    }),
    EffectsModule.forFeature([QuestionsEffects,QuestionDetailsEffects,AddQuestionEffects]),

  ],
  declarations:COMPONENTS,
  exports:COMPONENTS
})
export class QuestionsModule { }
