import { Component, Input } from '@angular/core';
import { QuestionDetails } from '../models';

@Component({
  selector: 'bc-answerd-question-details',
  template: `
    <mat-card-title-group>
      <mat-card-title>Would You Rather?</mat-card-title>
      <img *ngIf="question.avatarURL" [src]="question.avatarURL" />
    </mat-card-title-group>
    <mat-card-content>
      <p>
        <span> {{ question.optionOne.text }}</span>
        <span>Votes {{question.optionOne.votes.length}}</span>
        <span>{{CalculateVotesPrecentge(question.optionOne.votes.length)}} %</span>
      
      </p>
      <p>
      <span> {{ question.optionTow.text }}</span>
        <span>Votes {{question.optionTow.votes.length}}</span>
        <span>{{CalculateVotesPrecentge(question.optionTow.votes.length)}} %</span>
      </p>

      <p>
      <span> Peaples who voted on this poll</span>
        <span>Option 1 : {{CalculateQuestionPrecentge(question.optionOne.votes.length)}} %</span>
        <span>Option 2 : {{CalculateQuestionPrecentge(question.optionTow.votes.length)}} %</span>
      </p>
    </mat-card-content>
  `,
   styles: [
    `
      mat-card-title-group {
        margin-left: 0;
      }
      img {
        width: 4rem;
        aspect-ratio: 1;
        margin-left: 5px;
      }
      mat-card-content {
        margin: 1rem 0;
      }
      mat-card-content p {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      mat-card-content p >span{
        flex-basis:100%
      }
      mat-card-content p >span:nth-child(2){
        text-align:center
      }
      mat-card-content p >span:last-child{
        text-align:right
      }
      mat-icon {
        color: teal;
        cursor: pointer;
        font-size: 2rem;
      }
    `,
  ],
})
export class AnswerdQuestionDetailsComponent {
  @Input() question!: QuestionDetails;
  @Input() userCounts!:number;

  CalculateVotesPrecentge(len:number){
    return ((len/(this.question.optionOne.votes.length+this.question.optionTow.votes.length))*100).toFixed(2); 
  }

  CalculateQuestionPrecentge(len:number){
    return ((len/(this.userCounts))*100).toFixed(2); 
  }
}
