import { QuestionDetails } from './../models/question';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bc-unanswerd-question-details',
  template: `
    <mat-card-title-group>
      <mat-card-title>Would You Rather?</mat-card-title>
      <img *ngIf="question.avatarURL" [src]="question.avatarURL" />
    </mat-card-title-group>
    <mat-card-content>
      <p>
        {{ question.optionOne.text }}
        <button (click)="slecteOption.emit('optionOne')">
          <mat-icon>done_outline</mat-icon>
        </button>
      </p>
      <p>
        {{ question.optionTow.text }}
        <button (click)="slecteOption.emit('optionTwo')">
          <mat-icon>done_outline</mat-icon>
        </button>
      </p>
    </mat-card-content>
  `,
  styles: [
    `
      mat-card-title-group {
        margin-left: 0;
      }
      img {
        width: 4rem;
        aspect-ratio: 1;
        margin-left: 5px;
      }
      mat-card-content {
        margin: 1rem 0;
      }
      mat-card-content p {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      mat-card-content >p > button {
        border: none;
        background: transparent;
        padding-right: 1rem;
      }
      mat-icon {
        color: teal;
        cursor: pointer;
        font-size: 2rem;
      }
    `,
  ],
})
export class UnanswerdQuestionDetailsComponent {
  @Input() question!: QuestionDetails;
  @Output() slecteOption = new EventEmitter<string>();
}
