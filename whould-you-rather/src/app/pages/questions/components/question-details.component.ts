import { UserDetails } from 'src/app/pages/auth/models';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { QuestionDetails, QuestionOption, SaveQuestionAnswer } from '../models';

@Component({
  selector: 'bc-question-detail',
  template: `
    <mat-card *ngIf="!pending">
      <bc-answerd-question-details
        *ngIf="isAnswerd()"
        [question]="question"
        [userCounts]="userCounts"
      ></bc-answerd-question-details>

      <bc-unanswerd-question-details
        *ngIf="!isAnswerd()"
        [question]="question"
        (slecteOption)="Vote($event)"
      ></bc-unanswerd-question-details>
    </mat-card>
  `,
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        margin: 75px 0;
      }
      mat-card {
        width: 80%;
        max-width: 800px;
        padding: 2rem;
      }
    `,
  ],
})
export class QuestionDetailComponent {
  @Input() question!: QuestionDetails;
  @Input() user!: UserDetails;
  @Input() userCounts!: number;
  @Input() pending: boolean=false;
  @Output() vote = new EventEmitter<SaveQuestionAnswer>();
  isAnswerd(): Boolean {
    return this.user.answers[this.question.id] ? true : false;
  }
  Vote(option: string) {
    this.vote.emit({
      user: this.user,
      questionId: this.question.id,
      answer: option,
    });
  }
}
