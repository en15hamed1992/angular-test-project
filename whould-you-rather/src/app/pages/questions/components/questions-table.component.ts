import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Question } from '../models';

@Component({
  selector: 'bc-questions-table',
  template: `
    <button routerLink="/questions/add" mat-raised-button color="primary">
      Add Poll <mat-icon>add_circle_outline</mat-icon>
    </button>
    <br/>
    <br/>
    <table mat-table [dataSource]="questions$" class="mat-elevation-z8">
      <ng-container matColumnDef="id">
        <th mat-header-cell *matHeaderCellDef>Id</th>
        <td mat-cell *matCellDef="let question">{{ question.id }}</td>
        <td mat-footer-cell *matFooterCellDef>Total</td>
      </ng-container>

      <ng-container matColumnDef="author">
        <th mat-header-cell *matHeaderCellDef>Author</th>
        <td mat-cell *matCellDef="let question">
          {{ question.author }}
        </td>
        <td mat-footer-cell *matFooterCellDef></td>
      </ng-container>
      <ng-container matColumnDef="timestamp">
        <th mat-header-cell *matHeaderCellDef>Timestamp</th>
        <td mat-cell *matCellDef="let question">
          {{ question.timestamp }}
        </td>
        <td mat-footer-cell *matFooterCellDef>{{ count$ | async }}</td>
      </ng-container>

      <ng-container matColumnDef="actions">
        <th mat-header-cell *matHeaderCellDef></th>
        <td mat-cell *matCellDef="let question">
          <a [routerLink]="'/questions/' + question.id">
          <mat-icon>launch</mat-icon>
          </a>
        </td>
        <td mat-footer-cell *matFooterCellDef></td>
      </ng-container>
      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
      <tr mat-footer-row *matFooterRowDef="displayedColumns"></tr>
    </table>
  `,
  styles: [
    `
      table {
        width: 100%;
      }

      tr.mat-footer-row {
        font-weight: bold;
      }
    `,
  ],
})
export class QuestionTabelComponent {
    displayedColumns: string[] = ['id', 'author', 'timestamp', 'actions'];
   @Input() questions$!: Observable<readonly Question[]>;
   @Input()count$!: Observable<number>;
}
