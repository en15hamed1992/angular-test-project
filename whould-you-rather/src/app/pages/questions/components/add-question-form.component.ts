import { UserDetails } from 'src/app/pages/auth/models';
import { Observable } from 'rxjs';
import { AddOptions } from './../models/question';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AddQuestion } from '../models';
import { Store } from '@ngrx/store';

@Component({
  selector: 'bc-add-question-form',
  template: `
    <mat-card>
      <mat-card-title>Wohuld You Rather ?</mat-card-title>
      <mat-card-content>
        <form  [formGroup]="form" (ngSubmit)="submit()">
          <p>
            <mat-form-field>
              <input
                type="text"
                matInput
                placeholder="Option one"
                formControlName="optionOneText"
              />
            </mat-form-field>
          </p>
          <p>
            <mat-form-field>
              <input
                type="text"
                matInput
                placeholder="Option two"
                formControlName="optionTwoText"
              />
            </mat-form-field>
          </p>
        

          <p class="form-btn">
            <button type="submit" mat-button>Add</button>
          </p>
        </form>
      </mat-card-content>
    </mat-card>
  `,
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        margin: 72px 0;
      }

      .mat-form-field {
        width: 100%;
        min-width: 300px;
      }
      mat-card {
        padding: 2em;
        width: 30rem;
      }
      mat-card-title,
      mat-card-content {
        display: flex;
        justify-content: center;
      }

      .loginError {
        padding: 16px;
        width: 300px;
        color: white;
        background-color: red;
      }

      .form-btn {
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
      }
    `,
  ],
})
export class AddQuestionFormComponent {
    
 
  @Input() user!:UserDetails;
  
  @Output() submitted = new EventEmitter<AddOptions>();
  constructor() {
  }
  form: FormGroup = new FormGroup({
    optionOneText: new FormControl(''),
    optionTwoText: new FormControl(''),
    
  });
  submit() {
    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }
}
