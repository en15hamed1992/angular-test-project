import { SelectOption } from 'src/app/shared/models/select.option';
import { SaveQuestionAnswer } from './../models/question';
import { UserDetails } from 'src/app/pages/auth/models';
import { Observable, Subscription, map } from 'rxjs';
import { Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { QuestionDetails } from '../models';
import { QuestionDetailsActions } from '../actions';
import * as fromQuestion from '../reducers';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../../reducers';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'bc-question-details',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: ` <bc-question-detail
    *ngIf="(question$ | async) && (user$ | async)"
    [question]="(question$ | async)!"
    [user]="(user$ | async)!"
    [userCounts]="(userCounts$ | async)!"
    [pending]="(pending$|async)!"
    (vote)="SaveAnswer($event)"
  >
  </bc-question-detail>`,
})
export class QuestionDetailsPageComponent implements OnDestroy {
  question$: Observable<QuestionDetails | null>;
  user$: Observable<UserDetails | null>;
  userCounts$: Observable<number>;
  pending$: Observable<boolean>;
  accounts$: Observable<SelectOption<UserDetails>[]>;
  actionsSubscription: Subscription;
  constructor(private store: Store, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .pipe(
        map((params) =>
          QuestionDetailsActions.enter({ id: params['question_id'] })
        )
      )
      .subscribe((action) => store.dispatch(action));
    this.question$ = this.store.select(fromQuestion.selectQuestionDetails);
    this.user$ = store.select(fromAuth.selectUser);
    this.userCounts$ = store.select(fromAuth.selectAccountsCounts);
    this.accounts$ = store.select(fromAuth.selectUserOptions);
    this.pending$ =store.select(fromRoot.selectPending);
  }
  SaveAnswer(selectedAnswer: SaveQuestionAnswer) {
    const subscription = this.accounts$.subscribe((accounts) => {
      const acc = accounts.map((account) => {
        return {
          viewValue: account.viewValue,
          value: {
            id: account.value.id,
            name: account.value.name,
            avatarURL: account.value.avatarURL,
            answers: account.value.answers,
            questions: account.value.questions,
          },
        };
      });
      acc.map((account) => {
        if (account.value.id === selectedAnswer.user.id) {
          account.value.answers = {
            ...account.value.answers,
            [selectedAnswer.questionId]: selectedAnswer.answer,
          };
        }
        return account;
      });
      this.store.dispatch(
        QuestionDetailsActions.vote({ selectedAnswer, accounts: acc })
      );
    });
    subscription.unsubscribe();
  }
  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
  }
}
