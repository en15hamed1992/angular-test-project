import { AddOptions, AddQuestion } from './../models/question';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subscription, map } from 'rxjs';
import { UserDetails } from '../../auth/models';
import * as fromAuth from '../../auth/reducers';
import { Store } from '@ngrx/store';
import { AddQuestionActions } from '../actions';

@Component({
  selector: 'bc-add-question',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<bc-add-question-form
    [user]="(account$ | async)!"
    (submitted)="AddQuestion($event)"
  ></bc-add-question-form>`,
})
export class AddQuestionComponent {
  account$: Observable<UserDetails | null>;

  constructor(private store: Store) {
    this.account$ = this.store.select(fromAuth.selectUser);
  }

  AddQuestion(question: AddOptions) {
    const subscription = this.account$.subscribe((account) => {
      if (account) {
        this.store.dispatch(
          AddQuestionActions.Add({
            question: {
              optionOneText: question.optionOneText,
              optionTwoText: question.optionTwoText,
              author: account.id,
            },
          })
        );
      }
    });

    subscription.unsubscribe();
  }
}
