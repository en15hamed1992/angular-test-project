import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-questions-page',
  template: ' <router-outlet></router-outlet>',
  styles: [''],
})
export class QuestionsPageComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
