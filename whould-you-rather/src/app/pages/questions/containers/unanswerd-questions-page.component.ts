import { ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { UserDetails } from '../../auth/models';
import { UnanswerdQuestionsActions } from '../actions';
import { Question } from '../models';
import * as fromQuestions from '../reducers';
import * as fromAuth from '../../auth/reducers';
import { CdkTableDataSourceInput } from '@angular/cdk/table';

@Component({
  selector: 'bc-unanswerd-questions',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <bc-questions-table
    [questions$]="questions$"
    [count$]="count$"
    ></bc-questions-table>
  `,

  styles: [
    `
      table {
        width: 100%;
      }

      tr.mat-footer-row {
        font-weight: bold;
      }
    `,
  ],
})
export class UnanswerdQuestionsPageComponent implements OnDestroy {
  questions$: Observable<readonly Question[]>;
  count$: Observable<number>;
  user$: Observable<UserDetails | null>;
  subscription:Subscription;
  constructor(private store: Store) {
    this.questions$ = store.select(fromQuestions.selectQuestions);
    this.count$ = store.select(fromQuestions.selectQuestionsCount);
    this.user$ = store.select(fromAuth.selectUser);
   this.subscription= this.user$.subscribe((user) => {
      if (user) {
        this.store.dispatch(UnanswerdQuestionsActions.enter({ user }));
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

 
}
