import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { BackEndService } from 'src/app/shared/services/back-end.service';
import { AddQuestionActions, QuestionsApiActions } from '../actions';

@Injectable()
export class AddQuestionEffects {
    Add$ = createEffect(() => {
        return this.actions$.pipe(
          ofType(AddQuestionActions.Add),
          map((action) => action.question),
          switchMap((question) =>
            this.backEndService.AddQuestion(question).pipe(
              map((question) => QuestionsApiActions.SaveQuestionSucess({question})),
              catchError((error) =>
                of(QuestionsApiActions.SaveQuestionFailer({ error }))
              )
            )
          )
        );
      });

      addSuccess$= createEffect(()=>
    { return this.actions$.pipe(
      ofType(QuestionsApiActions.SaveQuestionSucess),
      tap(()=>this.router.navigate(['/questions/unaswerd']))
    ); },
    {dispatch:false}
  );
    constructor(
        private actions$: Actions,
        private backEndService: BackEndService,
        private router: Router
      ) {}
}