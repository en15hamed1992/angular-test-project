import { Question } from './../models/question';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { BackEndService } from 'src/app/shared/services/back-end.service';
import { Router } from '@angular/router';
import {
  AnswerdQuestionsActions,
  QuestionsApiActions,
  UnanswerdQuestionsActions,
} from '../actions';
import { of } from 'rxjs';

@Injectable()
export class QuestionsEffects {
  enterUnanswerd$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UnanswerdQuestionsActions.enter),
      map((action) => action.user),
      switchMap((user) =>
        this.backEndService.FetchQuestions().pipe(
          map((questions) =>
            Object.values(questions).sort((a, b) =>  b.timestamp - a.timestamp )
          ),
          map((questions) =>
            Object.values(questions).filter(
                         
              (question) =>  !(question.id in user.answers)
            )
          ),
          map((questions) =>
            QuestionsApiActions.FetchUnanswerdQuestionsSucess({
              questions: questions,
            })
          ),
          catchError((error) =>
            of(QuestionsApiActions.FetchUnanswerdQuestionsFailer({ error }))
          )
        )
      )
    );
  });

  enterAnswerd$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnswerdQuestionsActions.enter),
      map((action) => action.user),
      switchMap((user) =>
        this.backEndService.FetchQuestions().pipe(
          map((questions) =>
            Object.values(questions).sort((a, b) => b.timestamp - a.timestamp)
          ),
          map((questions) =>
            Object.values(questions).filter(
              (question) => question.id in user.answers
            )
          ),
          map((questions) =>
            QuestionsApiActions.FetchUnanswerdQuestionsSucess({
              questions: questions,
            })
          ),
          catchError((error) =>
            of(QuestionsApiActions.SaveQuestionAnswerFailer({ error }))
          )
        )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private backEndService: BackEndService,
    private router: Router
  ) {}
}
