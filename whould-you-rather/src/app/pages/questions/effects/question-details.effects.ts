import { map, switchMap, catchError, concatMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BackEndService } from 'src/app/shared/services/back-end.service';
import { QuestionDetailsActions, QuestionsApiActions } from '../actions';
import { of } from 'rxjs';

@Injectable()
export class QuestionDetailsEffects {
  enter$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(QuestionDetailsActions.enter),
      map((action) => action.id),
      switchMap((id) =>
        this.backEndService.FetchQuestionDetials(id).pipe(
          map((question) => {
            // console.log(question);

            if (question) {
              return QuestionsApiActions.FetchQuestionDetailsSucess({
                question,
              });
            }
            return QuestionsApiActions.FetchQuestionDetailsNotFound();
          }),
          catchError((error) =>
            of(QuestionsApiActions.FetchQuestionDetailsFailer({ error }))
          )
        )
      )
    );
  });

  vote$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(QuestionDetailsActions.vote),
      concatMap((action) => {
        return this.backEndService.VoteForAnswer(action.selectedAnswer).pipe(
        
          map(() =>
            QuestionsApiActions.SaveQuestionAnswerSucess({
              selectedAnswer: action.selectedAnswer,
              accounts: action.accounts
            })
          ),
          catchError((error) =>
            of(QuestionsApiActions.FetchQuestionDetailsFailer({ error }))
          )
        );
      })
    );
  });
  constructor(
    private actions$: Actions,
    private backEndService: BackEndService,
    private router: Router
  ) {}
}
