import { AddQuestionComponent } from './containers/add-questions.component';
import {  QuestionDetailsPageComponent } from './containers/question-details-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnswerdQuestionsPageComponent } from './containers/answerd-questions-page.component';
import { UnanswerdQuestionsPageComponent } from './containers/unanswerd-questions-page.component';

const routes: Routes = [
  { path: '',redirectTo:'/questions/unaswerd',pathMatch:'full' },
  { path: 'add', component: AddQuestionComponent },
  { path: 'answerd', component: AnswerdQuestionsPageComponent },
  { path: 'unaswerd', component: UnanswerdQuestionsPageComponent },
  { path: ':question_id', component: QuestionDetailsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionsRoutingModule {}
