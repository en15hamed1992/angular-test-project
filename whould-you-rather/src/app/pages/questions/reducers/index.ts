import * as fromQuestionsPage from './questions.page.reducer';
import * as fromQuestionsApi from './questions-api.reducer';
import * as fromQuestionDetailsApi from './question-details-api.reducer';
import * as fromRoot from '../../../reducers';
import * as fromAuth from '../../auth/reducers';

import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
export const questionsFetureKey = 'questions';

export interface QuestionsState {
  [fromQuestionsPage.questionsPageFeatureKey]: fromQuestionsPage.State;
  [fromQuestionsApi.questionApiFeatureKey]: fromQuestionsApi.State;
  [fromQuestionDetailsApi.questionDetailsApiFeatureKey]: fromQuestionDetailsApi.State;
}

export interface State extends fromRoot.State {
  [questionsFetureKey]: QuestionsState;
}

export function reducers(state: QuestionsState | undefined, action: Action) {
  return combineReducers({
    [fromQuestionsPage.questionsPageFeatureKey]: fromQuestionsPage.reducer,
    [fromQuestionsApi.questionApiFeatureKey]: fromQuestionsApi.reducer,
    [fromQuestionDetailsApi.questionDetailsApiFeatureKey]: fromQuestionDetailsApi.reducer

  })(state, action);
}

export const selectQuestionState =createFeatureSelector<QuestionsState>(questionsFetureKey);

export const selectQuestionsPageState= createSelector(
    selectQuestionState,
    (state)=>state.questionsPage
);
export const selectQuestionApiState= createSelector(
    selectQuestionState,
    (state)=>state.questionsAPI
);
export const selectQuestionDetailsApiState= createSelector(
  selectQuestionState,
  (state)=>state.questionsDetailsAPI
);
export const selectQuestions = createSelector(
    selectQuestionApiState,
    fromQuestionsApi.getQuestions
);
export const selectQuestionsCount = createSelector(
    selectQuestionApiState,
    fromQuestionsApi.getQuestionsCount
);
export const selectQuestionDetails = createSelector(
  selectQuestionDetailsApiState,
  fromQuestionDetailsApi.getQuestionDetails
);
export const selectQuestionDetailsIsAnswerd=createSelector(
selectQuestionDetails,
fromAuth.selectUser,
(question, user) => {
  if (user && question) {
    return (Object.keys(user?.answers!)
      .includes(question?.id!));
  }
  return null;
}
);
export const selectQuestionsPageError = createSelector(
    selectQuestionsPageState,
    fromQuestionsPage.getError
  );
  export const selectQuestionsPagePending = createSelector(
    selectQuestionsPageState,
    fromQuestionsPage.getPending
  );