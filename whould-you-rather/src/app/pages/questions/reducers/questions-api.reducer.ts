import { accountSelected } from './../../auth/actions/auth.actions';
import { createReducer, on } from '@ngrx/store';
import { QuestionsApiActions } from '../actions';
import { Question, Questions } from './../models/question';


export const questionApiFeatureKey = 'questionsAPI';

export interface State {
  questions: Question[];
  
}

export const initialState: State = {
  questions:[]
};

export const reducer=createReducer(
    initialState,
    on(QuestionsApiActions.FetchUnanswerdQuestionsSucess, (state,{questions}): State=>({
        ...state,
        questions,
    })),
    on(QuestionsApiActions.FetchAanswerdQuestionsSucess, (state,{questions}): State=>({
      ...state,
      questions,
  })),
  on(QuestionsApiActions.SaveQuestionAnswerSucess,(state,{selectedAnswer}):State=>({
    ...state,  
    questions:state.questions
    .map(q=>{
      if( q.id===selectedAnswer.questionId){
        if(selectedAnswer.answer==='optionOne'){
           [...q.optionOne.votes,selectedAnswer.user.id];
        }
        if(selectedAnswer.answer==='optionTwo'){
          [...q.optionTwo.votes,selectedAnswer.user.id];;
       }
      }
      return q;
     } )
    .filter(q=>q.id!==selectedAnswer.questionId),
  
  })),
  on(QuestionsApiActions.SaveQuestionSucess,(state,{question}): State=>(
    {
    ...state,
    questions:[...state.questions,question]
  }))

);

export const getQuestions=(state:State)=>state.questions;
export const getQuestionsCount=(state:State)=>Object.keys(state.questions).length;