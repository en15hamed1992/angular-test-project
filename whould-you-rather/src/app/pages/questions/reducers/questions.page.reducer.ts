import { createReducer, on } from '@ngrx/store';
import { AddQuestionActions, AnswerdQuestionsActions, QuestionDetailsActions, QuestionsApiActions, UnanswerdQuestionsActions } from '../actions';

export const questionsPageFeatureKey = 'questionsPage';

export interface State {
  error: string | null;
  pending: boolean;
}

export const initialState: State = {
  error: null,
  pending: false,
};

export const reducer = createReducer(
  initialState,

  on(
    QuestionsApiActions.FetchQuestionDetailsNotFound,
    QuestionsApiActions.FetchAanswerdQuestionsSucess,
    QuestionsApiActions.FetchQuestionDetailsSucess,
    QuestionsApiActions.FetchUnanswerdQuestionsSucess,
    QuestionsApiActions.SaveQuestionAnswerSucess,
    QuestionsApiActions.SaveQuestionSucess,
    (state): State => ({
      ...state,
      error: null,
      pending: false,
    })
  ),
  on(
    QuestionsApiActions.FetchAanswerdQuestionsFailer,
    QuestionsApiActions.FetchQuestionDetailsFailer,
    QuestionsApiActions.FetchUnanswerdQuestionsFailer,
    QuestionsApiActions.SaveQuestionAnswerFailer,
    QuestionsApiActions.SaveQuestionFailer,
    (state): State => ({
      ...state,
      error:state.error,
      pending: true,
    })
  ),

  on(
    AnswerdQuestionsActions.enter,
    UnanswerdQuestionsActions.enter,
    QuestionDetailsActions.enter,
    QuestionDetailsActions.vote,
    AddQuestionActions.Add,
    (state): State => ({
      ...state,
      error: null,
      pending: true,
    })
  )
);

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
