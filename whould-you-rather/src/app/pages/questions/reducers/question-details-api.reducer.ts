import { createReducer, on } from '@ngrx/store';
import { QuestionsApiActions } from '../actions';
import { QuestionDetails } from '../models';

export const questionDetailsApiFeatureKey = 'questionsDetailsAPI';

export interface State {
  question: QuestionDetails|null;
  
}

export const initialState: State = {
  question:null
};

export const reducer=createReducer(
    initialState,
    on(QuestionsApiActions.FetchQuestionDetailsSucess,(state,{question}):State=>({
        ...state,
        question
    })),

    on(QuestionsApiActions.SaveQuestionAnswerSucess,(state,{selectedAnswer}):State=>({
      ...state,
      question:{
        ...state.question!,
        optionOne:{
          ...state.question?.optionOne!,
          votes:selectedAnswer.answer==='optionOne'?
          [...state.question?.optionOne?.votes!,selectedAnswer.user.id]:
          [...state.question?.optionOne?.votes!]
        },
        optionTow:{
          ...state.question?.optionTow!,
          votes:selectedAnswer.answer==='optionTwo'?
          [...state.question?.optionTow?.votes!,selectedAnswer.user.id]:
          [...state.question?.optionTow?.votes!]
        }
      }
  })),
   

);

export const getQuestionDetails=(state:State)=>state.question;