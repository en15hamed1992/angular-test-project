import { SelectOption } from 'src/app/shared/models/select.option';
import { BackEndService } from './../../../shared/services/back-end.service';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, tap } from 'rxjs/operators';

import { AuthActions, AuthApiActions } from '../actions';
import { AuthService } from '../services/auth.service';
import { Credentials, UserDetails } from '../models';
import { LogoutConfirmationDialogComponent } from '../components';
import { UserActions } from 'src/app/core/actions';

@Injectable()
export class AuthEffects {
  enter$ = createEffect(() =>
    { return this.actions$.pipe(
      ofType(AuthActions.enter),
      switchMap(() =>
        this.backEndService.FetchUsers().pipe(
          map((users) => {
            const result: SelectOption<UserDetails>[] = [];
            Object.values(users as Object).forEach((account: UserDetails) => {
              result.push({
                value: account,
                viewValue: account.name,
              });
            });
            return result;
          }),
          map((options: SelectOption<UserDetails>[]) =>
            AuthApiActions.FetchAccountSuccess({ accounts: options })
          ),
          catchError((error) =>
            of(AuthApiActions.FetchAccountFailure({ error }))
          )
        )
      )
    ); }
  );
  accountSelected$= createEffect(()=>
    { return this.actions$.pipe(
      ofType(AuthActions.accountSelected),
      tap(()=>this.router.navigate(['/questions']))
    ); },
    {dispatch:false}
  );
  login$ = createEffect(() =>
    { return this.actions$.pipe(
      ofType(AuthActions.auth),
      map((action) => action.credentials),
      exhaustMap((auth: Credentials) =>
        this.authService.login(auth).pipe(
          map((user) => AuthApiActions.loginSuccess({ user })),
          catchError((error) => of(AuthApiActions.loginFailure({ error })))
        )
      )
    ); }
  );

  loginSuccess$ = createEffect(
    () =>
      { return this.actions$.pipe(
        ofType(AuthApiActions.loginSuccess),
        tap(() => this.router.navigate(['/']))
      ); },
    { dispatch: false }
  );

  loginRedirect$ = createEffect(
    () =>
      { return this.actions$.pipe(
        ofType(AuthApiActions.loginRedirect, AuthActions.logout),
        tap(() => {
          this.router.navigate(['/auth']);
        })
      ); },
    { dispatch: false }
  );

  logoutConfirmation$ = createEffect(() =>
    { return this.actions$.pipe(
      ofType(AuthActions.logoutConfirmation),
      exhaustMap(() => {
        const dialogRef = this.dialog.open<
          LogoutConfirmationDialogComponent,
          undefined,
          boolean
        >(LogoutConfirmationDialogComponent);

        return dialogRef.afterClosed();
      }),
      map((result) =>
        result ? AuthActions.logout() : AuthActions.logoutConfirmationDismiss()
      )
    ); }
  );

  logoutIdleUser$ = createEffect(() =>
    { return this.actions$.pipe(
      ofType(UserActions.idleTimeout),
      map(() => AuthActions.logout())
    ); }
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private backEndService: BackEndService,
    private router: Router,
    private dialog: MatDialog
  ) {}
}
