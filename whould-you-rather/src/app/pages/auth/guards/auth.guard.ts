import { switchMap, take } from 'rxjs/operators';
import { UserDetails } from 'src/app/pages/auth/models';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, map } from 'rxjs';
import * as fromAuth from '../reducers';
import { AuthApiActions } from '../actions';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  getUser(): Observable<UserDetails | null> {
    return this.store.select(fromAuth.selectUser);
  }
  isAuthenticated(user: UserDetails | null): boolean {
    if (user) {
      return true;
    }
    return false;
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.select(fromAuth.selectLoggedIn).pipe(
      map((authed) => {
        if (!authed) {
          this.store.dispatch(AuthApiActions.loginRedirect());
          return false;
        }

        return true;
      }),
      take(1)
    );
  }
}
