import { props, createAction } from '@ngrx/store';
import { SelectOption } from 'src/app/shared/models/select.option';
import { UserDetails } from '../models';

export const loginSuccess = createAction(
  '[Auth/API] Login Success',
  props<{ user: UserDetails }>()
);
export const FetchAccountSuccess = createAction(
  '[Auth/API] Fetch Accounts Success',
  props<{ accounts: SelectOption<UserDetails>[] }>()
);
export const loginFailure = createAction(
  '[Auth/API] Login Failure',
  props<{ error: any }>()
);
export const FetchAccountFailure = createAction(
  '[Auth/API] Fetch Account Failure',
  props<{ error: any }>()
);

export const loginRedirect = createAction('[Auth/API] Login Redirect');
