import { createAction, props } from '@ngrx/store';
import { Credentials, UserDetails } from '../models';

export const enter = createAction('[Auth Page] Enter');
export const accountSelected =
 createAction('[Auth Page] Account Selected'
 ,props<{user:UserDetails}>()
 );
export const auth = createAction(
  '[Auth Page] Auth',
  props<{ credentials: Credentials }>()
);
export const logout = createAction('[Auth] Logout');
export const logoutConfirmation = createAction('[Auth] Logout Confirmation');
export const logoutConfirmationDismiss = createAction(
  '[Auth] Logout Confirmation Dismiss'
);
