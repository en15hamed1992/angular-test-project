import { SharedModule } from './../../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { MaterialModule } from './../../matrial/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent, LogoutConfirmationDialogComponent } from './components';
import { AuthPageComponent } from './containers/auth-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountSelectionComponet } from './components/account-selection.component';
import { StoreModule } from '@ngrx/store';
import * as fromAuth from './reducers'
import { AuthEffects } from './effects';
import { EffectsModule } from '@ngrx/effects';

export const COMPONENTS = [
  AuthPageComponent,
  LoginFormComponent,
  LogoutConfirmationDialogComponent,
  AccountSelectionComponet,
  
];
@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    StoreModule.forFeature({
      name: fromAuth.authFeatureKey,
      reducer: fromAuth.reducers,
    }),
    EffectsModule.forFeature([AuthEffects]),

  ],
  declarations:COMPONENTS,
  exports:COMPONENTS
})
export class AuthModule { }
