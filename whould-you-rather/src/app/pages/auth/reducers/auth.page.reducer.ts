import { createReducer, on } from '@ngrx/store';
import { AuthActions, AuthApiActions } from '../actions';

export const authPageFeatureKey = 'authPage';

export interface State {
  error: string | null;
  pending: boolean;
}

export const initialState: State = {
  error: null,
  pending: false,
};

export const reducer = createReducer(
  initialState,
  on(AuthActions.auth, (state): State => ({
    ...state,
    error: null,
    pending: true,
  })),

  on(AuthApiActions.loginSuccess, (state): State => ({
    ...state,
    error: null,
    pending: false,
  })),
  on(AuthApiActions.loginFailure, (state, { error }): State => ({
    ...state,
    error,
    pending: false,
  })),

  on(AuthActions.enter, (state): State => ({
    ...state,
    error: null,
    pending: true,
  })),

  on(AuthApiActions.FetchAccountSuccess, (state): State => ({
    ...state,
    error: null,
    pending: false,
  })),
  on(AuthApiActions.FetchAccountFailure, (state, { error }): State => ({
    ...state,
    error,
    pending: false,
  })),


);

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
