import {
  createSelector,
  createFeatureSelector,
  Action,
  combineReducers,
} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import * as fromAuth from './auth.reducer';
import * as fromAuthPage from './auth.page.reducer';

export const authFeatureKey = 'auth';

export interface AuthState {
  [fromAuth.statusFeatureKey]: fromAuth.State;
  [fromAuthPage.authPageFeatureKey]: fromAuthPage.State;
}

export interface State extends fromRoot.State {
  [authFeatureKey]: AuthState;
}

export function reducers(state: AuthState | undefined, action: Action) {
  return combineReducers({
    [fromAuth.statusFeatureKey]: fromAuth.reducer,
    [fromAuthPage.authPageFeatureKey]: fromAuthPage.reducer,
  })(state, action);
}

export const selectAuthState = createFeatureSelector<AuthState>(authFeatureKey);

export const selectAuthStatusState = createSelector(
  selectAuthState,
  (state) => state.status
);
export const selectUser = createSelector(
  selectAuthStatusState,
  fromAuth.getUser
);
export const selectUserOptions = createSelector(
  selectAuthStatusState,
  fromAuth.getAccounts
);
export const selectUsers = createSelector(selectUserOptions, (options) =>
  options
    .map((option) => {
      return {
        name: option.value.name,
        avatarURL: option.value.avatarURL,
        questionsCount: option.value.questions.length,
        answersCount: Object.keys(option.value.answers).length,
      };
    })
    .sort((a, b) => {
      return   (b.answersCount + b.questionsCount) - ( a.answersCount + a.questionsCount);
    })
);
export const selectAccountsCounts = createSelector(
  selectUserOptions,
  (accounts) => accounts.length
);
export const selectLoggedIn = createSelector(selectUser, (user) => !!user);

export const selectAuthPageState = createSelector(
  selectAuthState,
  (state) => state.authPage
);
export const selectAuthPageError = createSelector(
  selectAuthPageState,
  fromAuthPage.getError
);
export const selectAuthPagePending = createSelector(
  selectAuthPageState,
  fromAuthPage.getPending
);
