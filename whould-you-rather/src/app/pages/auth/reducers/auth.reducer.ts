import { SelectOption } from './../../../shared/models/select.option';
import { createReducer, on } from '@ngrx/store';
import { AuthActions, AuthApiActions } from '../actions';
import { UserDetails } from '../models';
import { QuestionsApiActions } from '../../questions/actions';
import { EntityState } from '@ngrx/entity';

export const statusFeatureKey = 'status';

export interface State {
  user: UserDetails | null;
  accounts: SelectOption<UserDetails>[];
  
}

export const initialState: State = {
  user: null,
  accounts: [],
};

export const reducer = createReducer(
  initialState,
  on(
    AuthApiActions.loginSuccess,
    (state, { user }): State => ({ ...state, user })
  ),
  on(
    AuthApiActions.FetchAccountSuccess,
    (state, { accounts }): State => ({
      ...state,
      accounts,
    })
  ),
  on(AuthActions.logout, (): State => initialState),
  on(
    AuthActions.accountSelected,
    (state, { user }): State => ({
      ...state,
      user,
    })
  ),

  on(
    QuestionsApiActions.SaveQuestionAnswerSucess,
    (state, { selectedAnswer,accounts }): State => ({
      ...state,
      user: {
        ...state.user!,
        answers: {
          ...state.user?.answers,
          [selectedAnswer.questionId]: selectedAnswer.answer,
        },
      },
      accounts:accounts
    })
  ),
 
);

export const getUser = (state: State) => state.user;
export const getAccounts = (state: State) => state.accounts;
