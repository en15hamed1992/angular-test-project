import { Question } from 'src/app/pages/questions/models';
export interface Credentials {
  username: string;
}
export interface User{
  [username:string]:UserDetails
}
export interface UserDetails {
  id:string,
  name: string;
  avatarURL:string,
  answers:UserAnswer,
  questions:string[]
}
export interface UserAnswer{
  [questiondId:string]: string

}
