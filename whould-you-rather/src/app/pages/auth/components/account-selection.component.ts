import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SelectOption } from 'src/app/shared/models/select.option';
import { UserDetails } from '../models';

@Component({
  selector: 'bc-account-selction',
  template: `
    <mat-card>
      <mat-card-title>Select From Exsting Accounts</mat-card-title>
      <br>
      <mat-card-content>
        <form [formGroup]="form">
          <app-basic-select
            [loading]="loading"
            [label]="'accounts'"
            [options]="accounts"
            (valueChanged)="submit($event)"
          ></app-basic-select>
        </form>
      </mat-card-content>
    </mat-card>
  `,
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        margin: 72px 0;
      }

      mat-card{
        padding:2em;
        width:30rem;
        
      }
      mat-card-title,
      mat-card-content {
        display: flex;
        justify-content: center;
      }
    `,
  ],
})
export class AccountSelectionComponet {
  form: FormGroup = new FormGroup({
    account: new FormControl('JohnDoe'),
  });
  @Input() accounts!: SelectOption<UserDetails>[];
  @Input() loading = false;
  @Output() accountSelected = new EventEmitter<UserDetails>();
  submit($event: any) {
    const user = $event as UserDetails;
    this.accountSelected.emit(user);
  }
}
