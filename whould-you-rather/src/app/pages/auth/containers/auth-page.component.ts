import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAuth from '../reducers';
import { Credentials, UserDetails } from '../models';
import { AuthActions } from '../actions';

@Component({
  selector: 'bc-auth-page',
  changeDetection:ChangeDetectionStrategy.OnPush,
  template: `
    <bc-account-selction
      [loading]="(pending$ | async)!"
      [accounts]="(accounts$ | async)!"
      (accountSelected)="onSelectAccount($event)"
    ></bc-account-selction>
  `,
})
export class AuthPageComponent implements OnInit {
  pending$ = this.store.select(fromAuth.selectAuthPagePending);
  error$ = this.store.select(fromAuth.selectAuthPageError);
  accounts$ = this.store.select(fromAuth.selectUserOptions);
  account$ = this.store.select(fromAuth.selectUser);

  constructor(private store: Store) {}
  ngOnInit() {
    this.store.dispatch(AuthActions.enter());
  }
  onSelectAccount(account: UserDetails) {
    this.store.dispatch(
      AuthActions.accountSelected({
        user: account,
      })
    );
  }
  onSubmit(credentials: Credentials) {
    this.store.dispatch(AuthActions.auth({ credentials }));
  }
}
