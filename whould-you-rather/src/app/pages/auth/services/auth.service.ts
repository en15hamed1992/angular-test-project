import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Credentials, UserDetails } from '../models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  login({ username }: Credentials): Observable<any> {
    /**
     * Simulate a failed login to display the error
     * message for the login form.
     */
    if (username !== 'test' && username !== 'ngrx') {
      return throwError(() => 'Invalid username or password');
    }

    return of({
      id: '',
      name: 'User',
      avatarURL: '',
      questions:[],
      answers:[]
    });
  }

  logout() {
    return of(true);
  }
}
