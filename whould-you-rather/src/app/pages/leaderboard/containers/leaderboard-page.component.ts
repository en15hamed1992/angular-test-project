import { LeaderboardAccount } from './../models/leaderboard.model';
import { UserDetails } from 'src/app/pages/auth/models';
import { SelectOption } from './../../../shared/models/select.option';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Observable, map } from 'rxjs';
import * as fromAuth from '../../auth/reducers';

@Component({
    selector:'bc-leaderboard-page',
    template:`<bc-leaderboard-table
    [accounts]="(accounts$|async)!"
    ></bc-leaderboard-table>`
})
export class LeaderboardPageComponent implements OnInit{
    accounts$: Observable< LeaderboardAccount[]>;
    constructor(private store:Store){
        this.accounts$=this.store.select(fromAuth.selectUsers);
    }
    ngOnInit(): void {
       
    }

}