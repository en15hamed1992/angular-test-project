import { createAction } from '@ngrx/store';

const actionskey = '[Leaderboard Page]';

export const enter = createAction(`${actionskey} Enter`);
