import { LeaderboardTableComponent } from './components/leaderboard-table.component';
import { MaterialModule } from '../../matrial/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaderboardRoutingModule } from './leaderboard-routing.module';
import { LeaderboardPageComponent } from './containers/leaderboard-page.component';


export const COMPONENTS = [
  LeaderboardTableComponent,
  LeaderboardPageComponent
];
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    LeaderboardRoutingModule
    // StoreModule.forFeature({
    //  name:fromQuestions.questionsFetureKey,
    //  reducer:fromQuestions.reducers
    // }),
    // EffectsModule.forFeature([QuestionsEffects,QuestionDetailsEffects,AddQuestionEffects]),

  ],
  declarations:COMPONENTS,
  exports:COMPONENTS
})
export class LeaderboardModule { }
