import { LeaderboardAccount } from './../models/leaderboard.model';
import { Observable } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { SelectOption } from 'src/app/shared/models/select.option';
import { UserDetails } from '../../auth/models';

@Component({
  selector: 'bc-leaderboard-table',
  template: `
    <button routerLink="/questions/add" mat-raised-button color="primary">
      Add Poll <mat-icon>add_circle_outline</mat-icon>
    </button>
    <br />
    <br />
    <table mat-table [dataSource]="accounts" class="mat-elevation-z8">
      <ng-container matColumnDef="name">
        <th mat-header-cell *matHeaderCellDef>Name</th>
        <td mat-cell *matCellDef="let account">{{ account.name }}</td>
        <!-- <td mat-footer-cell *matFooterCellDef>Total</td> -->
      </ng-container>

      <ng-container matColumnDef="avatarURL">
        <th mat-header-cell *matHeaderCellDef>Image</th>
        <td mat-cell *matCellDef="let account">
          <img [src]="account.avatarURL" class="profile-pic" alt="" />
        </td>
        <!-- <td mat-footer-cell *matFooterCellDef></td> -->
      </ng-container>
      <ng-container matColumnDef="questionsCount">
        <th mat-header-cell *matHeaderCellDef>Questions Count</th>
        <td mat-cell *matCellDef="let account">
          {{ account.questionsCount }}
        </td>
        <!-- <td mat-footer-cell *matFooterCellDef></td> -->
      </ng-container>

      <ng-container matColumnDef="answersCount">
        <th mat-header-cell *matHeaderCellDef>Answers Count</th>
        <td mat-cell *matCellDef="let account">
          {{ account.answersCount }}
        </td>
        <!-- <td mat-footer-cell *matFooterCellDef></td> -->
      </ng-container>
      <ng-container matColumnDef="total">
        <th mat-header-cell *matHeaderCellDef>Total</th>
        <td mat-cell *matCellDef="let account">
          {{ account.answersCount + account.questionsCount }}
        </td>
        <!-- <td mat-footer-cell *matFooterCellDef></td> -->
      </ng-container>
      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
      <!-- <tr mat-footer-row *matFooterRowDef="displayedColumns"></tr> -->
    </table>
  `,

  styles: [
    `
      table {
        width: 100%;
      }

      tr.mat-footer-row {
        font-weight: bold;
      }
      .profile-pic {
        width: 3rem;
        aspect-ratio: 1;
        border-radius: 50%;
        margin: 0.5rem;
      }
    `,
  ],
})
export class LeaderboardTableComponent {
  displayedColumns: string[] = [
    'name',
    'avatarURL',
    'questionsCount',
    'answersCount',
    'total'
  ];
  @Input() accounts: LeaderboardAccount[] = [];
  constructor() {
    console.log(this.accounts);
  }
}
