export interface LeaderboardAccount{
    name:string,
    avatarURL:string,
    questionsCount:number,
    answersCount:number
}