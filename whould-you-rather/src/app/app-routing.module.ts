import { AuthGuard } from './pages/auth/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NotFoundPageComponent } from './core/containers';

const routes: Routes = [
  { path: '', redirectTo: '/auth', pathMatch: 'full' },

  {
    path:'auth',
    loadChildren:()=>import('./pages/auth/auth.module')
    .then((m)=>m.AuthModule)
  },
  {
    path:'leaderboard',
    loadChildren:()=>import('./pages/leaderboard/leaderboard.module')
    .then((m)=>m.LeaderboardModule),
    canActivate:[AuthGuard]
  },
  {
    path:'questions',
    loadChildren:()=>import('./pages/questions/questions.module')
    .then((m)=>m.QuestionsModule),
    canActivate:[AuthGuard]
  },
  {
    path: '**',
    component: NotFoundPageComponent,
    data: { title: 'Not found' },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
