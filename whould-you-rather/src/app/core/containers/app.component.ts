import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromRoot from '../../reducers';
import * as fromAuth from '../../pages/auth/reducers';
import { LayoutActions } from '../actions';
import { UserDetails } from 'src/app/pages/auth/models';
import { AuthActions } from 'src/app/pages/auth/actions';

@Component({
  selector: 'app-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <bc-layout>
      <bc-sidenav [open]="(showSidenav$ | async)!" (closeMenu)="closeSidenav()">
        <bc-nav-item
          (navigate)="closeSidenav()"
          *ngIf="loggedIn$ | async"
          routerLink="/questions/answerd"
          icon="computer"
          hint="Your answerd questions!"
        >
          Answerd Questions
        </bc-nav-item>

        <bc-nav-item
          (navigate)="closeSidenav()"
          *ngIf="loggedIn$ | async"
          routerLink="/questions/unaswerd"
          icon="help_outline"
          hint="Your unanswerd questions!"
        >
          Unanswerd Questions
        </bc-nav-item>
        <bc-nav-item
          (navigate)="closeSidenav()"
          *ngIf="loggedIn$ | async"
          routerLink="/leaderboard"
          icon="equalizer"
          hint="for all users"
        >
          Leaderboard
        </bc-nav-item>
        <p>
          <button class="signout-btn" (click)="logout()">
            <div>Sign Out</div>
            <mat-icon>clear</mat-icon>
          </button>
        </p>
      </bc-sidenav>
      <bc-toolbar
        [logedin]="(loggedIn$ | async)!"
        (openMenu)="openSidenav()"
        [title]="'Whould you rather?'"
      >
        <div *ngIf="loggedIn$ | async">
          <bc-account-details [user]="user$ | async"></bc-account-details>
        </div>
      </bc-toolbar>

      <div class="container">
        <router-outlet></router-outlet>
      </div>
      <app-loading-modal [isPending]="(pending$ | async)!"></app-loading-modal>
    </bc-layout>
  `,
  styles: [
    `
      .container {
        padding: 5%;
      }
      .signout-btn {
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 1.3rem;
        margin-top: 20rem;
        border: none;
        cursor: pointer;
        padding-inline: 5rem;
      }
    `,
  ],
})
export class AppComponent {
  showSidenav$: Observable<boolean>;
  loggedIn$: Observable<boolean>;
  pending$: Observable<boolean>;
  user$: Observable<UserDetails | null>;

  constructor(private store: Store) {
    this.showSidenav$ = this.store.select(fromRoot.selectShowSidenav);
    this.pending$ = this.store.select(fromRoot.selectPending);
    this.loggedIn$ = this.store.select(fromAuth.selectLoggedIn);
    this.user$ = this.store.select(fromAuth.selectUser);
  }

  closeSidenav() {
    this.store.dispatch(LayoutActions.closeSidenav());
  }

  openSidenav() {
    this.store.dispatch(LayoutActions.openSidenav());
  }

  logout() {
    this.store.dispatch(AuthActions.logoutConfirmation());
  }
}
