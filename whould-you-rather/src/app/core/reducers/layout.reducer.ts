import { createReducer, on } from '@ngrx/store';
import { AuthActions, AuthApiActions } from 'src/app/pages/auth/actions';
import { AddQuestionActions, AnswerdQuestionsActions, QuestionDetailsActions, QuestionsApiActions, UnanswerdQuestionsActions } from 'src/app/pages/questions/actions';

import { LayoutActions } from '../actions';
// import { AuthActions } from '@example-app/auth/actions';

export const layoutFeatureKey = 'layout';

export interface State {
  showSidenav: boolean;
  pending:boolean;
}

const initialState: State = {
  showSidenav: false,
  pending:false
};

export const reducer = createReducer(
  initialState,
  on(LayoutActions.closeSidenav, (): State => ({ showSidenav: false,pending:false })),
  on(LayoutActions.openSidenav, (): State => ({ showSidenav: true,pending:false })),
  on(AuthActions.logoutConfirmation, (): State => ({ showSidenav: false,pending:false })),

  on(
    AuthActions.enter,
    AddQuestionActions.Add,
    AnswerdQuestionsActions.enter,
    UnanswerdQuestionsActions.enter,
    QuestionDetailsActions.enter,
    QuestionDetailsActions.vote,
    (): State => ({ showSidenav: false,pending:true })
    ),
    on(
      AuthApiActions.FetchAccountFailure,
      AuthApiActions.FetchAccountSuccess,
      AuthApiActions.loginSuccess,
      AuthApiActions.loginFailure,
      QuestionsApiActions.FetchAanswerdQuestionsFailer,
      QuestionsApiActions.FetchAanswerdQuestionsSucess,
      QuestionsApiActions.FetchQuestionDetailsFailer,
      QuestionsApiActions.FetchQuestionDetailsNotFound,
      QuestionsApiActions.FetchQuestionDetailsSucess,
      QuestionsApiActions.FetchUnanswerdQuestionsFailer,
      QuestionsApiActions.FetchUnanswerdQuestionsSucess,
      QuestionsApiActions.SaveQuestionAnswerFailer,
      QuestionsApiActions.SaveQuestionAnswerSucess,
      QuestionsApiActions.SaveQuestionFailer,
      QuestionsApiActions.SaveQuestionSucess,
      (): State => ({ showSidenav: false,pending:false })

      )
);

export const selectShowSidenav = (state: State) => state.showSidenav;
export const selectPending = (state: State) => state.pending;
