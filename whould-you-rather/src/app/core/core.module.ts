import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { MaterialModule } from './../matrial/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './containers/app.component';
import {
  LayoutComponent,
  NavItemComponent,
  SidenavComponent,
  ToolbarComponent,
} from './components';
import { AccountDetailsComponent } from './components/account-details';

export const COMPONENTS = [
  AppComponent,
  NotFoundPageComponent,
  LayoutComponent,
  NavItemComponent,
  SidenavComponent,
  ToolbarComponent,
  AccountDetailsComponent
];
@NgModule({
  imports: [CommonModule, MaterialModule, RouterModule,SharedModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreModule {}
