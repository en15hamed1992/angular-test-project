import { Component, Input } from '@angular/core';
import { UserDetails } from 'src/app/pages/auth/models';

@Component({
  selector: 'bc-account-details',
  template: ` <div class="user-profile">
    <h3>Welcom {{ user?.name }}</h3>
    <img [src]="user?.avatarURL" alt="" />
  </div>`,
  styles:[`
   .user-profile {
        display: flex;
        align-items: center;
        gap: 1em;
      }
      .user-profile > img {
        width: 3rem;
        border-radius: 50%;
        aspect-ratio: 1;
      }`]
})
export class AccountDetailsComponent {
  constructor() {}
  @Input() user: UserDetails | null = null;
}
