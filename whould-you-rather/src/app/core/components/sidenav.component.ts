import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'bc-sidenav',
  template: `
    <mat-sidenav
      #sidenav
      [opened]="open"
      (keydown.escape)="sidenav.close()"
      (closedStart)="closeMenu.emit()"
      disableClose
    >
      <mat-nav-list>
        <p>
          <button
            (click)="sidenav.close()"
            mat-icon-button
            color="primary"
          >
            <mat-icon>clear</mat-icon>
          </button>
        </p>
        <ng-content></ng-content>
      </mat-nav-list>
    </mat-sidenav>
  `,
  styles: [
    `
      mat-sidenav {
        width: 300px;
      }
      mat-nav-list > p:first-child {
        text-align: right;
        padding: 2em;
      }
    `,
  ],
})
export class SidenavComponent {
  @Input() open = false;
  @Output() closeMenu = new EventEmitter();
}
