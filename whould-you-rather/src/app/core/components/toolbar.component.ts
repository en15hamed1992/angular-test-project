import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'bc-toolbar',
  template: `
    <mat-toolbar color="primary">
      <div class="title" >
      <button
      *ngIf="logedin"
      mat-icon-button (click)="openMenu.emit()" aria-label="menu">
        <mat-icon>menu</mat-icon>
      </button>
      <h2>{{title}}</h2>
      </div>

      <ng-content></ng-content>
    </mat-toolbar>
  `,
  styles:[`

  mat-toolbar{
    display:flex;
    justify-content:space-between
  }
  mat-toolbar>.title{
    display:flex;
    align-items:center
  }
  
  `]
})
export class ToolbarComponent {
  @Input() title='';
  @Input() logedin=false;
  @Output() openMenu = new EventEmitter();
}
